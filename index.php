<?php include __DIR__ . '/defines.php'; ?>
<html>
    <head>
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Exo+2|Rajdhani|Righteous" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css" rel="stylesheet" type="text/css" />
        <title>IPD11 - PHP - Project</title>
    </head>
    <body>
        <div class="container">
            <?php include INCLUDES_DIR . '/header.php'; ?>
            <main role="main">
                <?php include Lib\Request::contentFile(); ?>
            </main>
            <?php include INCLUDES_DIR . '/footer.php'; ?>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" ></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="/assets/js/courseScript.js"></script>
        <?php
        $scripts = \Lib\Request::getScript();
        if (sizeof($scripts)) {
            foreach ($scripts as $path) {
                ?><script src="<?php echo $path; ?>"></script><?php
            }
        }
        ?>

    </body>
</html>