<h1>Course Registration</h1>
<div id="status-message"></div>

<form id="course-reg-form" action="/api/course/addStudent" method="POST">
    <div class="form-group">
        <label for="studentid">Student</label>
        <div id="studentlist"></div>
        <small id="studentidHelp" class="form-text text-muted">List of students.</small>
    </div>
    <div class="form-group ">
        <label for="courseid">Course</label>
        <div id="courselist"></div>
        <small id="courseidHelp" class="form-text text-muted">List of courses.</small>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>

<script id="studentlist-handlbebar-tmpl" type="text/x-handlebars-template">
    {{#if data}}
    <select class="form-control" name="studentid" id="studentid" 
        aria-describedby="studentidHelp"
        data-validation="required">
            <option value="">Select student</option>
            {{#each data}}
               <option value="{{id}}">{{first_name}}</option> 
            {{/each}}
    </select>    
    {{/if}}
</script>
<script id="courselist-handlbebar-tmpl" type="text/x-handlebars-template">
    {{#if data}}
    <select class="form-control" name="courseid" id="courseid" 
                aria-describedby="courseidHelp"
                data-validation="required">
            <option value="">Select Course</option>
            {{#each data}}
               <option value="{{id}}">{{name}}</option> 
            {{/each}}
    </select>    
    {{/if}}
</script>
<?php
\Lib\Request::addScript('/assets/js/course-registration.js');