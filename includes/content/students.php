<h1>List of Students</h1>
<table class="table table-striped table-hover table-bordered">

    <tr>
        <th class="bg-primary trHead ">First name</th>
        <th class="bg-primary trHead">Last name</th>
        <th class="bg-primary trHead">Date of birth</th>
    </tr>
    <tbody id='studentlist'></tbody>
</table>


<form action="/api/student/studentAdd" method="post" id="student-form">
    <!--<form action="/api/student/studentAdd" method="post" id="student-form" onsubmit="reloadPage()">-->
    <div id="status-message"></div>
    <h1>Add Student</h1>

    <div class="form-group">
        <label for="first_name">First name</label>
        <input type="text" class="form-control" name="first_name" id="first_name"
               data-validation="length" 
               data-validation-length="2-64"
               pattern="[A-Za-z]{2,64}"
               data-validation-error-msg="Your First Name should be between 2 and 64 characters without empty spaces, please look at the example above." 
               aria-describedby="first_nameHelp" 
               placeholder="First name" >
        <small id="first_nameHelp" class="form-text text-muted">First Name (i.e. John)</small>
    </div>
    <div class="form-group">
        <label for="last_name">Last name</label>
        <input type="text" class="form-control" name="last_name" id="last_name"
               data-validation="length" 
               data-validation-length="2-64"
               pattern="[A-Za-z]{2,64}"
               data-validation-error-msg="Your Last Name should be between 2 and 64 characters without empty spaces, please look at the example above." 
               aria-describedby="last_nameHelp" 
               placeholder="Last name"  >
        <small id="last_nameHelp" class="form-text text-muted">Last Name (i.e. Smith)</small>
    </div>
    <div class="form-group">
        <label for="dob">DoB</label>
        <input type="text" class="form-control " name="dob" id="dob" 
               data-validation="date"
               data-validation-format="yyyy-mm-dd"
               data-validation-error-msg="Incorrect Date Format, please look at the example above." 
               aria-describedby="dobHelp" 
               placeholder="Date of birth" 
               pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" >
        <small id="dobHelp" class="form-text text-muted">Date of birth (i.e. YYYY-MM-DD)</small>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>

<script id="student-handlbebar-tmpl" type="text/x-handlebars-template">
    {{#if data}}
    {{#each data}}
        <tr>
            <td>{{first_name}}</td>
            <td>{{last_name}}</td>
            <td>{{dob}}</td>
        </tr>
    {{/each}}
    {{/if}}
</script>

<script src=""></script>
<?php
\Lib\Request::addScript('/assets/js/student.js');
