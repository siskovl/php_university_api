<h1>List of Courses</h1>
<table class="table table-striped table-hover table-bordered" id="course-table">
    <tr>
        <th class="bg-primary trHead ">Code</th>
        <th class="bg-primary trHead ">Name</th>
        <th class="bg-primary trHead ">Description</th>
        <th class="bg-primary trHead "># of students</th>
    </tr>
    <tbody id='coruselist'></tbody>
</table>

<form id="course-form" action="/api/course/courseAdd" method="POST">
    <h1>Add Course</h1>
    <div id="status-message"></div>
    <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control" name="code" id="code" 
               aria-describedby="codeHelp" 
               placeholder="Course code"
               data-validation="length" 
               data-validation-length="1-8"
               data-validation-error-msg="Incorrect Course Code, please look at the example above." 
               >
        <small id="codeHelp" class="form-text text-muted">Course code (e.i PHP100 )</small>
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" 
               aria-describedby="nameHelp" 
               placeholder="Course name"
               data-validation="length" 
               data-validation-length="1-25"
               data-validation-error-msg="Incorrect Course Name, please look at the example above." 
               >
        <small id="nameHelp" class="form-text text-muted">Course name (e.i PHP)</small>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" 
                  aria-describedby="descriptionHelp" 
                  placeholder="Course description"
                  data-validation="length" 
                  data-validation-length="1-255"
                  data-validation-error-msg="Incorrect Course Description, please look at the example above." 
                  ></textarea>
        <small id="descriptionHelp" class="form-text text-muted">Course description (e.i. PHP For Beginners)</small>
    </div>
    <button type="submit" id="button" class="btn btn-primary">Add</button>
</form>

<script id="coruse-handlbebar-tmpl" type="text/x-handlebars-template">
    {{#if data}}
    {{#each data}}
    <tr>
    <td>{{code}}</td>
    <td>{{Course_name}}</td>
    <td>{{description}}</td>
    <td>{{Count_students}}</td>
    </tr>
    {{/each}}
    {{/if}}
</script>

<?php
\Lib\Request::addScript('/assets/js/course.js');
