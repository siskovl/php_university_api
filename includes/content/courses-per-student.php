<h1>Students by course</h1>
<div id='status-message'></div>
<form class="form-group" id="studentbycourse-form" action="/api/student/by/course" method="GET">    
    <div class="row">
        <div class="form-group mx-sm-3 mb-2">
            <label for="code" class="sr-only">Course code</label>
            <input type="text" class="form-control" name="code" id="code" data-validation="length" 
                   data-validation-length="1-15" placeholder="Course code">
        </div>
    </div>
    <button id="btnGo" type="submit" class="btn btn-primary mb-2">Go</button>
</form>
<div id='codeStudentPerClass'></div>
<script id="studentpercourse-handlbebar-tmpl" type="text/x-handlebars-template">
    <table id="courseTable" class="table table-bordered">
    <thead>
    <tr>
    <th>Code & # of students</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Date of birth</th>

    </tr>
    </thead>
    <tbody>
    {{#if data}}
    <tr>
    <td rowspan='{{data.rowspan}}'>{{data.courseCode}} {{data.numOfStudents}}</td>
    </tr>
    {{#each data.list}}
    <tr>
    <td>{{this.first_name}}</td>
    <td>{{this.last_name}}</td>
    <td>{{this.dob}}</td>
    </tr>
    {{/each}}
    {{else}}
    <tr>
    <td colspan='4'>No data</td>
    </tr>
    {{/if}}
    </tbody>

    </table>
</script>

<script src=""></script>
<?php
\Lib\Request::addScript('/assets/js/student-per-courses.js');
