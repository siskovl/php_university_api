function formHandler(messageContainerId, formId, successCallback, errorCallback)
{
    var statusMessageContainer = $('#'+messageContainerId);
    
    $.validate({
        form: '#'+formId,
        onSuccess: function(formElement){
            
            $.ajax({
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: formElement.serialize(),
                dataType: 'json',
                error: function(a, b, c) {
                    if (errorCallback) {
                        errorCallback(a,b,c);
                    }
                    else {
                        statusMessageContainer.html(alertBox('danger', 'Error: '+b));
                        statusMessageContainer.find('.alert').alert();
                    }
                },
                success: function(response) {
                    // reset form
                    formElement[0].reset();
                    
                    if (successCallback) {
                        successCallback(response);
                        statusMessageContainer.html(alertBox('success', response.message));
                    }
                    else {
                        // display output
                        if (response.error)
                        {
                            statusMessageContainer.html(alertBox('danger', response.error));
                        }
                        statusMessageContainer.find('.alert').alert();
                        statusMessageContainer.html(alertBox('success', response.message));
                    }
                }
            });
            
            return false;
        }
    });
}

function itemsList(url, tmplId, destId)
{
    $.ajax({
        url: url,
        dataType: 'json',
        success: function(resp) {
            var handlebarTmpl = Handlebars.compile($('#'+tmplId).html());
            var context = {data: resp.data};
            console.log(context);
            $('#'+destId).html(handlebarTmpl(context));
        }
    });
}

function alertBox(type, message)
{
    var html = [];
    html.push('<div class="alert alert-' + (type || 'info') + ' alert-dismissible fade show" role="alert">');
    html.push('<button type="button" class="close" data-dismiss="alert" aria-label="Close">');
    html.push('<span aria-hidden="true">&times;</span>');
    html.push('</button>');
    html.push('<div class="message">' + message + '</div>');
    html.push('</div>');
    return html.join('');
}