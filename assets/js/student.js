function getStudentList()
{
    itemsList('/api/student/studentList', 'student-handlbebar-tmpl', 'studentlist');
}

$(document).ready(function(){
    getStudentList();
    formHandler('status-message', 'student-form', getStudentList);
});