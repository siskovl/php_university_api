function getStudentsPerCourse(resp)
{
    var handlebarTmpl = Handlebars.compile($('#studentpercourse-handlbebar-tmpl').html());
    var context = {data: resp.data};
    console.log('getStudentsPerCourse: ', context);
    $('#codeStudentPerClass').html(handlebarTmpl(context));
}

$(document).ready(function(){
    formHandler('status-message', 'studentbycourse-form', getStudentsPerCourse); 
    
});

