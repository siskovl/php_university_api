<?php

namespace MyApi;

abstract class ContextProcessorServiceAbstract
{
    private static $dbo = null;
    protected $output = array();

    abstract public function setUriParts(array $uriParts);
    
    abstract public function execute();
    
    public final function getOutputAsArray()
    {
        return $this->output;
    }
    
    public function getDbo(){
        if(self::$dbo === null){
            $dbSettings = new \Database\DbSettings('mysql', 'localhost', 'yourUsernameHere', 'yourPasswordHere');
            self::$dbo = new \Database\Dbo($dbSettings);
        }
        return self::$dbo;
    }
    
}

