<?php

namespace MyApi\Services;

class Student extends \MyApi\ContextProcessorServiceAbstract {

    private $uriParts = array();

    public function setUriParts(array $uriParts) {
        $this->uriParts = $uriParts; // api/contac/processdata --> here we have [processdata]
    }

    public function execute() {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0]) {
            if (method_exists($this, $this->uriParts[0])) {
                $this->{$this->uriParts[0]}(); // if it exists then execute the path
            } else {
                $this->output = array('error' => 'Method ' . $this->uriParts[0]); // if it doesnt exist then trow an error
            }
        } else {
            $this->output = array('error' => 'Illegal request.');
        }
    }

    private function studentAdd() {

        $statement = 'INSERT INTO dbName.tbName(first_name,last_name, dob) VALUES('
                . $this->getDbo()->quote($_POST['first_name'])
                . ',' . $this->getDbo()->quote($_POST['last_name'])
                . ',' . $this->getDbo()->quote($_POST['dob'])
                . ')';
        $this->getDbo()->query($statement);

        $this->output = array(
            'success' => true,
            'message' => 'New Student Successfully Added!'
        );
    }

    private function studentList() {

        $stm = "SELECT first_name, last_name, dob FROM dbName.tbName";


        $row = $this->getDbo()->loadAssocList($stm);
        $this->output = array(
            'data' => $row,
            'success' => true,
            'message' => 'Successfully processed!' . json_encode($_POST)
        );
    }

    private function getStudents() {

        $stm = "SELECT * FROM dbName.tbName";


        $row = $this->getDbo()->loadAssocList($stm);


        $this->output = array(
            'data' => $row
        );
    }

    private function by() {
        if (sizeof($this->uriParts) && $this->uriParts[0] && $this->uriParts[1]) {
            if ($this->uriParts[1] == "course") {
                $stm = "SELECT students.first_name, students.last_name, students.dob "
                        . "FROM dbName.courses AS courses "
                        . "JOIN dbName.student_courses ON dbName.student_courses.course_id = courses.id "
                        . "LEFT JOIN dbName.students ON dbName.students.id = student_courses.student_id "
                        . "WHERE courses.code = "
                        . $this->getDbo()->quote($_GET['code'])
                        . "";

                $row = $this->getDbo()->loadAssocList($stm);
                $numOfStudents = sizeof($row);



                $this->output = array(
                    'data' => array(
                        'courseCode' => $_GET['code'],
                        'numOfStudents' => $numOfStudents,
                        'rowspan' => $numOfStudents+1,
                        'list' => $row
                    ),
                    'success' => true,
                    'message' => 'Successfully processed.'
                );
            }
        }
    }
}