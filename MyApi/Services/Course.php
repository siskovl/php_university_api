<?php

namespace MyApi\Services;

class Course extends \MyApi\ContextProcessorServiceAbstract {

    private $uriParts = array();

    public function setUriParts(array $uriParts) {
        $this->uriParts = $uriParts; // api/contac/processdata --> here we have [processdata]
    }

    public function execute() {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0]) {
            if (method_exists($this, $this->uriParts[0])) {
                $this->{$this->uriParts[0]}(); // if it exists then execute the path
            } else {
                $this->output = array('error' => 'Method ' . $this->uriParts[0]); // if it doesnt exist then trow an error
            }
        } else {
            $this->output = array('error' => 'Illegal request.');
        }
    }

    // if we change this to vote: ---> Email or rewrite to database
    private function courseAdd() {

        $statement = 'INSERT INTO dbName.tbName(code, name, description) VALUES('
                . $this->getDbo()->quote($_POST['code']). ',' 
                . $this->getDbo()->quote($_POST['name']). ',' 
                . $this->getDbo()->quote($_POST['description']) 
                . ')'; // how to insert data to database              
        $this->getDbo()->query($statement);

        $this->output = array(
            'success' => true,
            'message' =>  'Course Added!' 
        );
    }
    
    private function courseList(){

        $stm = "SELECT MAX(courses.name) AS Course_name, COUNT(student_courses.student_id) AS Count_students, courses.code, courses.description
        FROM dbName.courses AS courses
        LEFT JOIN dbName.student_courses ON dbName.student_courses.course_id = courses.id
        GROUP BY courses.id";
        
        
        $row = $this->getDbo()->loadAssocList($stm);
        $this->output = array(
            'data' => $row
        );
    }  
    
    
    private function getCourses() {

        $stm = "SELECT * FROM dbName.courses";


        $row = $this->getDbo()->loadAssocList($stm);
        
        
        $this->output = array(
            'data' => $row            
        );
    }
    
    private function addStudent(){
        
        $values = array();
        $values[] = $this->getDbo()->quote($_POST['studentid']);
        $values[] = $this->getDbo()->quote($_POST['courseid']);

        $statement = 'INSERT INTO dbName.student_courses(student_id, course_id) VALUES('. implode(',', $values).')'; // how to insert data to database 
        //die($statement);
        $this->getDbo()->query($statement);

        $this->output = array(
            'success' => true,
            'message' =>  'Registered' 
        );
    }
}
